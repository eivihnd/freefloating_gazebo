#include <ros/ros.h>
#include <tinyxml.h>
#include <string>
#include <iostream>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Point.h>
#include <freefloating_gazebo/WaterCurrentLookup.h>
#include <vector>


geometry_msgs::Vector3 samples;
geometry_msgs::Vector3 size;
std::vector<std::vector<std::vector<geometry_msgs::Vector3> > > current;
std::string interpolator;

double convertToGridAxis(double axis_position, double axis_size, double axis_samples, bool calculate_z_axis = false,double axis_origin = 0)
{
    // TODO: ADD TEST IF ANY VALUE IS 0
    // TODO: ADD AXIS ORIGIN IF NECESSARY
    
    // (axis_sample -1) to accomodate for array indexing 0- (n-1)
    if (!calculate_z_axis){
        return (0.5 - axis_position/axis_size) * (axis_samples-1);
    }
    else
    {
        // Origin is at the surface, and underwater vehicles will be below the surface.
        // Z=0 in the current array is at the surface, and an incrementation of the z value
        // will correspond to increased depth. Therefore, the z-value from the world must be
        // inverted in order to work correctly.
        
        return (-axis_position / axis_size) * (axis_samples-1);

    }
}

geometry_msgs::Point getRelativeGridPosition(geometry_msgs::Point worldPosition)
{
    geometry_msgs::Point relative_position;
    // Calculate relative position for each axis
    relative_position.x = convertToGridAxis(worldPosition.x, size.x, samples.x);
    relative_position.y = convertToGridAxis(worldPosition.y, size.y, samples.y);
    relative_position.z = convertToGridAxis(worldPosition.z, size.z, samples.z, true);
    
    // Check whether all of the positions are within the bounderies of the defined size.
    // Points outside the defined grid will return the closest point within the grid.
    
    bool objectOutsideDefinedVolume = false;
    
    if (relative_position.x > (samples.x -1))
    {
        relative_position.x = samples.x - 1;
        objectOutsideDefinedVolume = true;
    }
    else if (relative_position.x < 0)
    {
        relative_position.x = 0;
        objectOutsideDefinedVolume = true;
    }
    
    if (relative_position.y > (samples.y -1))
    {
        relative_position.y = samples.y - 1;
        objectOutsideDefinedVolume = true;
    }
    else if (relative_position.y < 0)
    {
        relative_position.y = 0;
        objectOutsideDefinedVolume = true;
    }
    
    if (relative_position.z > (samples.z -1))
    {
        relative_position.z = samples.z - 1;
        objectOutsideDefinedVolume = true;
    }
    else if (relative_position.z < 0)
    {
        relative_position.z = 0;
        objectOutsideDefinedVolume = true;
    }
    if (objectOutsideDefinedVolume)
    {
        ROS_WARN_STREAM_THROTTLE(5, "Object outside defined volume/area. Nearest defined point will be used. Object position: [" << worldPosition.x << "," << worldPosition.y << "," << worldPosition.z << "] Relative position: [" << relative_position.x << "," << relative_position.y << "," << relative_position.z << "]");
    }
    
    return relative_position;
    
}


// Service function
bool getWaterCurrent (freefloating_gazebo::WaterCurrentLookup::Request &req, freefloating_gazebo::WaterCurrentLookup::Response &res)
{
    ROS_INFO_STREAM_ONCE("Running WaterCurrentLookup serivce");
    //ROS_INFO_STREAM_THROTTLE(1, "Requested position: [" << req.coordinate.x << "," << req.coordinate.y << "," << req.coordinate.z << "]");
    
    geometry_msgs::Point pos = getRelativeGridPosition(req.coordinate);
    
    
    /// Interpret file and fill array
    
    if (interpolator == "nearest_neighbour")
    {
        // Simple implementation of nearest neighbour.
        res.current = current[  (int)round(pos.x)  ][  (int)round(pos.y)  ][  (int)round(pos.z)  ];
    }
    else
    {
        ROS_WARN_STREAM("Interpolator not correctly defined. Chech launchfile parameters. Defaults to nearest_neighbour");
        interpolator = "nearest_neighbour";
    }
    

    
    return true;
}


// Function to dynamically allocate a null multidimensional array.
std::vector<std::vector<std::vector<geometry_msgs::Vector3> > > fillArray(int x, int y, int z)
{
    std::vector<std::vector<std::vector<geometry_msgs::Vector3> > > current_array_init;
    current_array_init.resize(x);
    
    
    for(int i = 0; i < x; ++i)
    {
        current_array_init[i].resize(y);
        for(int j = 0; j < y; ++j)
        {
            current_array_init[i][j].resize(z);
            for (int k = 0; k < z; ++k)
            {
                geometry_msgs::Vector3 current_init;
                current_array_init[i][j][k] = current_init;
            }
        }
    }
    
    ROS_INFO_STREAM("Water current array initiated with size: [" << current_array_init.capacity() << "," << current_array_init[0].capacity() << "," << current_array_init[0][0].capacity() << "]");
                    
    return current_array_init;
}

int main(int argc, char **argv)
{
    
    // Initiate node
    ros::init(argc, argv, "freefloating_gazebo_current_simulator");
    ros::NodeHandle nh;
    
    // Create publisher for multiarray current data ********* MUST BE CHANGED TO CUSTOM MESSAGE TYPE *******
    ros::ServiceServer server = nh.advertiseService("get_water_current", getWaterCurrent);
    
    
    
    //// PARAMETERS  ////
    
    // Define parameter names
    const std::string PARAM_FILE_NAME = "~current_file_name";
    const std::string PARAM_INTERPOLATOR = "~interpolator";
    
    // Assign default parameter values (if not set)
    std::string default_file_name = "current.xml";
    std::string default_interpolator = "nearest_neighbour";
    
    // Read parameters
    std::string filename;
    
    bool param_found = ros::param::get(PARAM_FILE_NAME, filename);
    if (!param_found)
    {
        ROS_WARN_STREAM("Could not find parameter " << PARAM_FILE_NAME << ". Using default value: " << default_file_name);
        filename = default_file_name;
    }
    
    param_found = ros::param::get(PARAM_INTERPOLATOR, interpolator);
    if (!param_found)
    {
        ROS_WARN_STREAM("Could not find parameter " << PARAM_INTERPOLATOR << ". Using default value: " << default_interpolator;
                        interpolator = default_interpolator);
    }
    
    
    //// Load current file ////
    
    TiXmlDocument doc(filename);
    bool fileLoaded = doc.LoadFile();
    
    if(!fileLoaded)
    {
        ROS_ERROR_STREAM("Could not load water current file \"" << filename);
    }
    else
    {
        // File loaded sucessfully, parse file
        
        
        // Define grid
        TiXmlElement* sample_size_xml = doc.FirstChildElement("freefloating_gazebo_current_simulator")->FirstChildElement("grid")->FirstChildElement("samples");  // GENERATES ERROR
        samples.x = std::atoi( sample_size_xml->FirstChildElement("x")->GetText());
        samples.y = std::atoi( sample_size_xml->FirstChildElement("y")->GetText());
        samples.z = std::atoi( sample_size_xml->FirstChildElement("z")->GetText());
        
        
        
        ROS_INFO_STREAM("Sample size: x=" << samples.x << " y=" << samples.y << " z=" << samples.z);
        
        // Define area size
        TiXmlElement* size_xml = doc.FirstChildElement("freefloating_gazebo_current_simulator")->FirstChildElement("grid")->FirstChildElement("size");
        size.x = std::atoi( size_xml->FirstChildElement("x")->GetText());
        size.y = std::atoi( size_xml->FirstChildElement("y")->GetText());
        size.z = std::atoi( size_xml->FirstChildElement("z")->GetText());
        
        
        ROS_INFO_STREAM("Area size: x=" << size.x << " y=" << size.y << " z=" << size.z);
        
        // Create multidimensial array
        
        current = fillArray(size.x, size.y, size.z);
        
        
        // Load all points into multidimensional array of type geometry_msgs::Vector3
        
        TiXmlElement* current_xml = doc.FirstChildElement("freefloating_gazebo_current_simulator")->FirstChildElement("current");
        
        for(TiXmlElement* e = current_xml->FirstChildElement("point"); e!= NULL; e = e->NextSiblingElement() )
        {
            TiXmlElement* coordinate = e->FirstChildElement("coordinates");
            int x_pos = std::atoi( coordinate->FirstChildElement("x")->GetText());
            int y_pos = std::atoi( coordinate->FirstChildElement("y")->GetText());
            int z_pos = std::atoi( coordinate->FirstChildElement("z")->GetText());
            
            TiXmlElement* current_vector = e->FirstChildElement("current_vector");
            double current_x = std::atof( current_vector->FirstChildElement("x")->GetText());
            double current_y = std::atof( current_vector->FirstChildElement("y")->GetText());
            double current_z = std::atof( current_vector->FirstChildElement("z")->GetText());
            
            geometry_msgs::Vector3 current_data;
            
            current_data.x = current_x;
            current_data.y = current_y;
            current_data.z = current_z;
            
            current[x_pos][y_pos][z_pos] = current_data;
            
            ROS_INFO_STREAM("Point added: ["<< x_pos << "," << y_pos << "," << z_pos << "] Current: [" << current_x << "," << current_y << "," << current_z << "]");
            
        }
        
    } // end if xml file loaded
    
    
    
    // Let ROS take over
    ros::spin();
    return 0;
    
}